################################################################################
# INFO/Configuration
	#  Uploads resourcestat (a subset of all land uploads), maps
	#  countries to FAO codes, and loads area data from FAO.
	#  Population data is used to estimate area for artificial surfaces.

	#  UPDATE FILE PATHS
		# Find and replace [NFA 2019] path below with new path for NFA Edition
	 	# "C:\Users\laurel hanscom\Dropbox (Footprint Network)\GFN NFA\NFA 2019"

	#  INPUT FILES:
		#		"C:\\Users\\laurel hanscom\\Dropbox (Footprint Network)\\GFN NFA\\NFA 2019\\2. Datasets\\Land\\Upload Data\\ResourceStat2019_All Data.csv"

	#  OUTPUT TABLES
		#   `area_19_baseline_v1_raw`.`resourcestat_raw`           #UPDATE
		#   `area_19_baseline_v1_sql`.`bel_lux_ratio_tmp`          #UPDATE
		#   `area_19_baseline_v1_sql`.`resourcestat`               #UPDATE
		#   `area_19_baseline_v1_sql`.`yearval`                    #UPDATE
		#   `area_19_baseline_v1_ult`.`resourcestat`               #UPDATE

	#  REQUIRED TABLES
		#   `country_data.country_matrix_2019`						#UPDATE
		#	 	`faostat_18_v3_ult`.`popstat_ult`			  			#UPDATE

	#  FLAGS
	 #		Data Source Flags
	  #   E:  Expert sources from FAO (including other divisions)                                                                        #UPDATE
		#   F:  FAO estimate																																																							 #UPDATE
		#		Fc:	???                                                                                                            						 #UPDATE
		#   Fm: Manual Estimation                                                                                                          #UPDATE
		#   I:  Country data reported by International Organizations where the country is a member (Semi-official) - WTO, EU, UNSD, etc.   #UPDATE
		#   Q:  Official data reported on FAO Questionnaires from countries                                                                #UPDATE
		#   W:  Data reported on country official publications or web sites (Official) or trade country files                              #UPDATE
	 #  	GFN flags (assigned by this script)
	  #		OUT: value had been flagged as exceptional and replaced
		#		FIL: value interpolated or extrapolated
		#		R: real value reported by corine
	################################################################################

# BEGIN PRE-LOAD
	TEE C:\Users\laurel hanscom\Dropbox (Footprint Network)\GFN NFA\NFA 2019\2. Datasets\Land\Upload Data\dot out log files\Landarea.sql
	SET @total_time=UNIX_TIMESTAMP() ;
	SELECT NOW() as 'Data Load Started';

	CREATE SCHEMA IF NOT EXISTS `area_19_baseline_v1_raw`;				#UPDATE
	CREATE SCHEMA IF NOT EXISTS `area_19_baseline_v1_sql` ;				#UPDATE
	CREATE SCHEMA IF NOT EXISTS `area_19_baseline_v1_ult` ;				#UPDATE

	USE `area_19_baseline_v1_sql` ;																#UPDATE

# BEGIN LOAD RAW

	# Populate variables for later use
		SET @msg = 'ERROR: UNMAPPED COUNTRY';
		SELECT MAX(end_year) FROM country_data.country_matrix_2019 INTO @default_end_year\p;		#UPDATE

	# Create a table with all the years as rows. This table built as a variable year to use for indexing

		DROP TABLE IF EXISTS `yearval`\p;
		CREATE TABLE `yearval` (year int(5))\p;

		INSERT INTO yearval Values
		(1961),(1962),(1963),(1964),(1965),(1966),(1967),(1968),(1969),(1970),
		(1971),(1972),(1973),(1974),(1975),(1976),(1977),(1978),(1979),(1980),
		(1981),(1982),(1983),(1984),(1985),(1986),(1987),(1988),(1989),(1990),
		(1991),(1992),(1993),(1994),(1995),(1996),(1997),(1998),(1999),(2000),
		(2001),(2002),(2003),(2004),(2005),(2006),(2007),(2008),(2009),(2010),
		(2011),(2012), (2013),(2014),(2015)\p;																#UPDATE

	 # Create raw table and insert data from csv

			DROP TABLE IF EXISTS `area_19_baseline_v1_raw`.`resourcestat_raw`\p;
			CREATE TABLE  `area_19_baseline_v1_raw`.`resourcestat_raw`(
			  `country_code` int(5) NOT NULL,
			  `country` varchar(70) NOT NULL,
			  `FAO_item_code` INT(5) NOT NULL,
			  `item` varchar(40) NOT NULL,
			  `element_ID` varchar(40) NOT NULL,
			  `element` varchar(40) NOT NULL,
			  `year_code` int(4) NOT NULL,
			  `year` int(4) NOT NULL,
			  `unit` varchar(15) NOT NULL,
			  `value` double DEFAULT NULL,
			  `flag` VARCHAR(4) DEFAULT NULL,
			PRIMARY KEY  (`country`,`FAO_item_code`, `year`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1\p;

			LOAD DATA LOCAL INFILE "C:\\Users\\laurel hanscom\\Dropbox (Footprint Network)\\GFN NFA\\NFA 2019\\2. Datasets\\Land\\Upload Data\\ResourceStat2019_All Data.csv"
				INTO TABLE `area_19_baseline_v1_raw`.`resourcestat_raw` CHARACTER SET  latin1
				FIELDS TERMINATED BY "," OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY "\r\n" IGNORE 1 LINES
				(country_code, country,FAO_item_code,item,element_ID,element,year_code,year,unit,@value,flag)
				SET  `value` = nullif(@value,'')\p;

# BEGIN SQL

		USE `area_19_baseline_v1_sql`\p;

	# Create table and copy from raw
		DROP TABLE IF EXISTS `area_19_baseline_v1_sql`.`resourcestat`\p;					#UPDATE

		CREATE TABLE `area_19_baseline_v1_sql`.`resourcestat` (										#UPDATE
			`country` varchar(70) NOT NULL,
			`country_code` smallint(5) NOT NULL,
			`item` varchar(120) NOT NULL,
			`FAO_item_code` smallint(6) NOT NULL,
			`element` varchar(120) NOT NULL,
			`element_ID` int(6) NOT NULL,
			`unit` varchar(15) NOT NULL,
			`year` smallint(4) NOT NULL,
			`value` double DEFAULT NULL,
			`flag` varchar(5) DEFAULT NULL,
		  PRIMARY KEY (`country`,`FAO_item_code`, `year`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1\p;

		INSERT INTO `area_19_baseline_v1_sql`.`resourcestat`
			SELECT country, country_code, item, FAO_item_code, element, element_ID, unit, year, value, flag
			FROM `area_19_baseline_v1_raw`.`resourcestat_raw`;

	# Standardize country names with `country_names_2019`(GFN table with UN names/codes)
		#1B.Set country code values to 0 in prep for country mapping
			UPDATE `resourcestat` SET country_code = 0\p;

		#2A.Map from country_data.country_names_2019 table							#UPDATE
			UPDATE `resourcestat` resourcestat
			LEFT JOIN country_data.country_names_2019 cn ON cn.name = resourcestat.country		#UPDATE
			SET resourcestat.country_code = if(cn.code is null,0,cn.code)\p;

		#2B.Delete intentionally dropped countries
			DELETE FROM `resourcestat` WHERE country_code = -1\p;

		#2C.Check to ensure that countries are either correctly mapped or intentionally dropped
			SELECT IF ((SELECT count(country_code) FROM `resourcestat` WHERE country_code = 0) > 0,@msg,'COUNTRY MAPPING OK') ;
			SELECT DISTINCT country FROM `resourcestat` WHERE country_code = 0\p;

		#2D.Aggregate where country_code is the same
			DROP TABLE IF EXISTS `rs_intm1`\p;
			CREATE TABLE `rs_intm1` LIKE `resourcestat`\p;
			INSERT INTO `rs_intm1` SELECT country, country_code, item,  FAO_item_code, element, element_ID, unit, year, sum(value), flag FROM `resourcestat` GROUP BY country_code, FAO_item_code, element_ID, year ;

		#2B.Re-insert back into original table and drop tmp table
			TRUNCATE `resourcestat`\p;
			INSERT INTO `resourcestat` SELECT * FROM `rs_intm1`\p;
			DROP TABLE IF EXISTS `rs_intm1`\p;

		ALTER TABLE `resourcestat` drop primary key, add primary key (`country_code`,`FAO_item_code`, `year`) \p;

		#2F.Remap country codes to country names to get correct names (for user accessability only)
			UPDATE `resourcestat` rs
			LEFT JOIN country_data.country_names_2019 cn ON cn.code = rs.country_code		#UPDATE
			SET rs.country =  cn.name
			WHERE (cn.primary_name = 1);

	# Re-Naming Items
		#3A. Rename "Forest" item to "Forest area" (template matching purposes)
			UPDATE `resourcestat` SET item = 'Forest area' WHERE FAO_item_code = 6663;

		#3B. Rename "Cropland" item to "Arable land and Permanent crops" (template matching purposes) ##Laurel note to self: should we just update the template so this doesn't have to happen?
			UPDATE `resourcestat` SET item = 'Arable land and Permanent crops' WHERE FAO_item_code = 6620;

		#3C. Rename "Inland waters" item to "Inland water" (template matching purposes) ##Laurel note to self: should we just update the template so this doesn't have to happen?
						UPDATE `resourcestat`
						SET item = 'Inland water' WHERE FAO_item_code = 6680;

		#3D. Rename "Land under perm. meadows and pastures" item to "Permanent meadows and pastures" (template matching purposes) ##Laurel note to self: should we just update the template so this doesn't have to happen?
										UPDATE `resourcestat`
										SET item = 'Permanent meadows and pastures' WHERE FAO_item_code = 6655;

		#3D. Rename "Land under tenp. meadows and pastures" item to "Temporary meadows and pastures" (template matching purposes) ##Laurel note to self: should we just update the template so this doesn't have to happen?
										UPDATE `resourcestat`
										SET item = 'Temporary meadows and pastures' WHERE FAO_item_code = 6633;

		#4. Fill 'temporary meadows and pastures' data based on average ratio between it and 'arable land'
			INSERT INTO resourcestat
			SELECT country, country_code, item, FAO_item_code, element, element_ID, unit, year, arable_land*ratio, "RFIL"
			FROM (	SELECT country, country_code, year, a.value as `arable_land`
					FROM resourcestat a
					LEFT JOIN (	SELECT country_code, year, value
								FROM resourcestat b
								WHERE FAO_item_code = 6633
							  ) b USING(country_code, year)
					WHERE a.FAO_item_code = 6621 AND b.value IS NULL
				  ) a
			JOIN (	SELECT country_code, a.item, a.FAO_item_code, a.element, a.element_ID, a.unit, sum(a.value)/sum(b.value) as ratio
					FROM resourcestat a
					JOIN resourcestat b USING(country_code, year)
					WHERE a.FAO_item_code = 6633 AND b.FAO_item_code = 6621
					GROUP BY country_code
				  ) b USING(country_code) ;

		#5. Remove '6633 - Temporary meadows and pastures' from '6620 - Arable land and Permanent crops'
			UPDATE `resourcestat` rs
			RIGHT JOIN (SELECT * FROM area_19_baseline_v1_sql.resourcestat where FAO_item_code = 6633) tmp ON rs.country_code = tmp.country_code AND rs.year = tmp.year
			SET rs.value = (rs.value - tmp.value)
			WHERE rs.FAO_item_code=6620;

	# Delete the zero Forest data for 1961 to 1989
		Delete from `resourcestat` a where a.FAO_item_code=6663 and a.year <1990 \p;

	### Delete the zero Forest data Belgium and Luxembourg between 1990 to 1999

		Delete from `resourcestat` a where a.country_code in (255,256) and a.year <2000 \p;

	### Split Belgium Luxembourg before 2000

		# Check Luxembourg inland water for years 2000 to 2013; if null, insert variable value
			DELETE FROM `area_19_baseline_v1_sql`.`resourcestat`
			WHERE country_code = 256 AND item = 'Inland Water' AND value is null;

			INSERT IGNORE INTO `area_19_baseline_v1_sql`.`resourcestat`
			SELECT 'Luxembourg',256,a.item,a.FAO_item_code,a.element,a.element_ID,a.unit,a.year,b.value-a.value,'BLS'
			FROM `area_19_baseline_v1_sql`.`resourcestat` a
			LEFT JOIN `area_19_baseline_v1_sql`.`resourcestat` b USING(item)
			WHERE a.country_code = 255 AND b.country_code = 15 AND a.item = 'Inland Water' and b.year = 1999 and a.year > 1999;

		# Start split
			drop table if exists `area_19_baseline_v1_sql`.`Bel_Lux_ratio_tmp` \p;
			create table `area_19_baseline_v1_sql`.`Bel_Lux_ratio_tmp` (
			`FAO_item_code` smallint (5) not null,
			`bel_perc` double,
			`lux_perc` double) \p;

			insert into `area_19_baseline_v1_sql`.`Bel_Lux_ratio_tmp`
			select a.FAO_item_code, ifnull(a.value,0)/(ifnull(a.value,0)+ifnull(b.value,0)), ifnull(b.value,0)/(ifnull(a.value,0)+ifnull(b.value,0))
			from `resourcestat` a, `resourcestat` b
			where a.FAO_item_code=b.FAO_item_code
			and a.country_code=255
			and b.country_code=256
			and a.year=b.year
			and a.year=2000 \p;

			insert into `resourcestat`
			SELECT
			    'Belgium' as country,
			    255 as country_code,
			    a.item,
				a.FAO_item_code,
				a.element,
				a.element_ID,
				a.unit,
				a.year,
				a.value*b.`bel_perc`,
				'BLS' as flag
				FROM
			    `resourcestat` a,
				`area_19_baseline_v1_sql`.`Bel_Lux_ratio_tmp` b
				where
			    a.FAO_item_code=b.FAO_item_code
				and a.year<2000
				and a.country_code=15 \p;

			insert into `resourcestat`
			SELECT
			    'Luxembourg' as country,
			    256 as country_code,
			    a.item,
				a.FAO_item_code,
				a.element,
				a.element_ID,
				a.unit,
				a.year,
				a.value*b.`lux_perc`,
				'BLS' as flag
				FROM
			    `resourcestat` a,
				`area_19_baseline_v1_sql`.`Bel_Lux_ratio_tmp` b
				where
			    a.FAO_item_code=b.FAO_item_code
				and a.year<2000
				and a.country_code=15 \p;

			# Delete the Bel-Lux values to avoid double counting
			DELETE from `resourcestat` a where a.country_code = 15 \p;

	### Update resourcestat sql table with GFN_flag.
		#Call it 'R' if it is any given FAO flag, and keep any others create by us up to this point in the GFN_flag column.

		alter table resourcestat add column GFN_flag varchar(4) after flag\p;

		update resourcestat
		set GFN_flag='R'\p;

		update resourcestat
		set GFN_flag=flag where flag NOT IN ('W','Q','Fm','F','I','E','A')\p;

# BEGIN ULT

		#Move copy final sql table to ult schema
		USE `area_19_baseline_v1_ult`\p;
		DROP TABLE IF EXISTS `resourcestat`\p;
		CREATE TABLE `resourcestat` like `area_19_baseline_v1_sql`.`resourcestat`\p;
		INSERT INTO `resourcestat`
			SELECT a.* FROM `area_19_baseline_v1_sql`.`resourcestat` a
			LEFT JOIN `country_data`.`country_matrix_2019` b							#UPDATE
			USING(country_code)
			WHERE a.year >= b.Start_year
			AND a.year <= b.End_year \p;

# END
	SELECT NOW() as 'Data Load Ended' ;
	select (UNIX_TIMESTAMP() - @total_time) as 'Total elapsed time in seconds';
	NOTEE
